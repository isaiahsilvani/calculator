package com.example.calculator

import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.calculator.databinding.MainActivityBinding

class MainActivity : AppCompatActivity(R.layout.main_activity) {
     lateinit var binding: MainActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        setContentView(binding.root)
    }



    fun initViews() {
        binding = MainActivityBinding.inflate(layoutInflater)

        with(binding){
            // function for adding numbers
            fun addNumber(num: CharSequence) {
                var textData = textView3.text.toString()
                textData += num.toString()
                textView3.text = textData
            }
            val numberButtons: List<Button> = listOf(
                zeroBtn, oneBtn, twoBtn, threeBtn, fourBtn,
                fiveBtn, sixBtn, sevenBtn, eightBtn, nineBtn,
                )
            // Loop through buttons, add to TextView functionality
            for (btn in numberButtons) {
                btn.setOnClickListener {
                    addNumber(btn.text)
                }
            }
            // Clear textView
            clearBtn.setOnClickListener { textView3.text = "" }
            // Add operator to TextView functionality
            val operatorButtons: List<Button> = listOf(
                plusBtn, minusBtn, multiplyBtn, divideBtn
            )
            for (btn in operatorButtons) {
                // If theres an operator already at end of textView, don't add
                btn.setOnClickListener {
                    val operators: List<Char> = listOf('*', '-', '*', '/')
                    if (textView3.text != "") {
                        if (!(operators.contains(textView3.text.last()))) {
                            addNumber(btn.text)
                        }
                    }

                }
            }
            // EQUAL FUNCTIONALITY STARTS HERE
            equalBtn20.setOnClickListener {
                // SCREW PARSING LOL
                val mathStr = textView3.text.toString()
                val operators: List<String> = listOf("*", "-", "+", "/")
                var mathList: MutableList<String>? = mutableListOf()
                var currentNum: String = ""

                for (i: Int in 0..(mathStr.length - 1)) {
                    // IF CURRENT NUMBER IS NOT AN OPERATOR, ADD IT TO CURRENTNUM STRING
                    val num = mathStr[i].toString()
                    if (!(operators.contains(num)) && !(i.toInt() == mathStr.length - 1)) {
                        currentNum += num
                    } else if (i.toInt() == mathStr.length - 1){
                        // If we're at the last character, just add it to the jawnskie
                        currentNum += num
                        mathList?.add(currentNum)

                    } else {
                        mathList?.add(currentNum)
                        mathList?.add(num)
                        currentNum = ""
                    }
                    println("$mathList --- the list before...")
                    print(mathList is MutableList<String>?)
                } // FOR LOOP ENDS HERE
                println("$mathList --- the list after")
                println("Type of MutableList<String>?: ${mathList is MutableList<String>?}")
                fun solveArray(list: MutableList<String>?): String {

                    var result: Double? = null


                    if (list != null) {

                        var solveList = list
                        val firstNum = list[0].replace(" ", "").toDouble()
                        val operate = list[1]
                        val secondNum = list[2].replace(" ", "").toDouble()


                        if (operate == "+") {
                            result = firstNum + secondNum
                        }
                        if (operate == "-") {
                            result = firstNum - secondNum
                        }
                        if (operate == "*") {
                            result = firstNum * secondNum
                        }
                        if (operate == "/") {
                            result = firstNum / secondNum
                        }

                        solveList.removeAt(0)
                        solveList.removeAt(0)
                        solveList.set(0, result.toString())
                        println("Result: $result.    List: $solveList   ")

                        // If there is another element past Third Num, remove elements at front and replace with
                        // the result, repeat process. Else, just return result
                        if (solveList.size >= 3) {
                            println("HIT")
                            solveArray(solveList)
                        } else {
                            println("$result is the result")
                        }

                    }
                    // If the result is x.0, remove the 0

                    return result.toString()
                }
                textView3.text = solveArray(mathList)
                // THINGS ARE GOOD RIGHT HERE...
            } // THE EVENT LISTNER ENDS HERE
        }
    }
}

