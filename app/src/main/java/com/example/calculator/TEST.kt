package com.example.calculator

class TEST {

    fun solveArray(list: MutableList<String>?): String {

        var result: Int? = null


        if (list != null) {

            var solveList = list
            val firstNum = list[0].toInt()
            val operate = list[1]
            val secondNum = list[2].toInt()


            if (operate == "+") {
                result = firstNum + secondNum
            }
            if (operate == "-") {
                result = firstNum - secondNum
            }
            if (operate == "*") {
                result = firstNum * secondNum
            }

            solveList.removeAt(0)
            solveList.removeAt(0)
            solveList.set(0, result.toString())
            println("Result: $result.    List: $solveList   ")

            // If there is another element past Third Num, remove elements at front and replace with
            // the result, repeat process. Else, just return result
            if (solveList.size >= 3) {
                println("HIT")
                solveArray(solveList)
            } else {
                println("$result is the result")
            }

        }
        return result.toString()
    }


}